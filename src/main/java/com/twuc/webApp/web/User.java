package com.twuc.webApp.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

public class User extends ResourceSupport{
    private Long id;
    private String firstName;
    private String lastName;

    public User() {
    }

    @JsonProperty("id")
    public Long getUserId() {
        return id;
    }

    public User(Long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}

